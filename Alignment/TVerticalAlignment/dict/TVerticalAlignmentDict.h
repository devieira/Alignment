#ifndef DICT_TVERTICALALIGNMENTDICT_H 
#define DICT_TVERTICALALIGNMENTDICT_H 1

// Include files
#include "TVerticalAlignment/TVerticalAlignment.h"
#include "TVerticalAlignment/OTYAlignMagOff.h"
#include "TVerticalAlignment/ITYAlignMagOff.h"
#include "TVerticalAlignment/TTYAlignMagOff.h"

#endif // DICT_TVERTICALALIGNMENTDICT_H
