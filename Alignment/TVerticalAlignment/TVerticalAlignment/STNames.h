// Include files

#ifndef STNAMES_H
#define STNAMES_H

#include <vector>
#include <string>
#include <map>

#include "TROOT.h"
#include "TString.h"
#include <iostream>
#include "TSystem.h"
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TPad.h"

#include "RooFit.h"
#include "RooRealVar.h"
#include "RooDataHist.h"
#include "RooDataSet.h"

using namespace std;

/** @class STNames STNames.h macros/STNames.h
 *   *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2010-11-22
 */
class STNames {
public: 
  struct SectNames
  {
    Int_t UniqueSector;
    Int_t UniqueSectorTM;
    Int_t NbSensors;
    TString Nickname;
    Int_t ITno;
    TString TT;
    TString Box;
    TString Layer;
    TString Region;
    Int_t Sectno;
    Int_t YPos;
  };
  struct LayerNames
  {
    Int_t UniqueLayer;
    TString Nickname;
    Int_t ITno;
    TString TT;
    TString Box;
    TString Layer;
  };
  struct BiLayerNames
  {
    Int_t UniqueBiLayer;
    TString Nickname;
    Int_t ITno;
    TString TT;
    TString Box;
    TString BiLayer;
  };
  struct BoxNames
  {
    Int_t UniqueBox;
    TString Nickname;
    Int_t ITno;
    TString Box;
  };
  
  typedef std::vector<std::string> Vector;
  typedef std::map<std::string,SectNames> MapSectNames;
  typedef std::map<std::string,LayerNames> MapLayerNames;
  typedef std::map<std::string,BiLayerNames> MapBiLayerNames;
  typedef std::map<std::string,BoxNames> MapBoxNames;
    
  /// Standard constructor
  STNames() {}; 
  STNames(const char* detType);
  inline virtual ~STNames() {}; ///< Destructor
  
  STNames::Vector* GetSectors();
  STNames::Vector* GetLayers();
  STNames::Vector* GetBiLayers();
  STNames::Vector* GetBoxes();
  STNames::SectNames* GetSectNames(string const& sector);
  STNames::LayerNames* GetLayerNames(string const& layer);
  STNames::BiLayerNames* GetBiLayerNames(string const& bilayer);
  STNames::BoxNames* GetBoxNames(string const& box);
  string GetGlobalName();
  string GetBoxName(string const& sector);
  string GetLayerName(string const& sector);
  string GetBiLayerName(string const& sector);
  Int_t GetUniqueSector(string const& sector);
  Int_t GetUniqueLayer(string const& layer);
  Int_t GetUniqueBox(string const& box);
  Int_t GetNbSensors(string const& sector);
  Int_t GetYPos(string const& sector);
    
protected:

private:
  void Init();
  void InitITMapNames(Int_t const& ITno, Int_t const& Sectno, const char* Box, const char* Layer);
  void InitTTMapNames(const char* TT, const char* Region, const char* Layer, Int_t const& Sectno);
  
  Vector m_Sectors;
  Vector m_Layers;
  Vector m_BiLayers;
  Vector m_Boxes;
  Vector m_Global;
  MapSectNames m_MapSectNames;
  MapLayerNames m_MapLayerNames;
  MapBiLayerNames m_MapBiLayerNames;
  MapBoxNames m_MapBoxNames;
  TString m_detType;
  Bool_t m_init;
  
  //ClassDef(STNames,1);
};

#endif
