// Include files

#ifndef ST2DPLOT_H
#define ST2DPLOT_H

#include <map>
#include "TVerticalAlignment/STNames.h"
#include "TH2D.h"

using namespace std;

/** @class ST2DPlot ST2DPlot.h macros/ST2DPlot.h
 *   *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2010-11-23
 */
class ST2DPlot {
public: 
  /// Standard constructor
  ST2DPlot() {}; 
  ST2DPlot(const char* detType, const char* name, const char* title, const char* plotType="Sector");
  inline virtual ~ST2DPlot() {}; ///< Destructor

  void Fill(string const& sector, double const& value);
  void Draw(Option_t* option);
  TH2D* Histogram() {return m_hmap;}
protected:

private:
  TString m_detType;
  TString m_plotType;

  std::map<int,double> m_XMap;
  std::map<int,double> m_YMap;
  std::map<int,double> m_NbSectMap;
  STNames* m_names;
  TH2D* m_hmap;

  int    m_NBinX;
  int    m_NBinY;
  double m_LowX;
  double m_LowY;
  double m_UpX;
  double m_UpY;

  void PlotLabels();
  void PlotBoxes();
  void InitMaps();
  //ClassDef(ST2DPlot,1);
};

#endif
