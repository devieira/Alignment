#!/usr/bin/env python

from __future__ import division

import os

std_AligWork_dir = '/group/online/AligWork/'
std_alignment_dir = '/group/online/alignment/'
std_references = os.path.expandvars('$ALIGNMENTMONITORINGROOT/files/ConstantsReferences.txt')


##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description ="Macro to know who triggered the update")
    parser.add_argument('-r','--runs', help='run numbers, default is all the availables', nargs=2, type=int,default=[172933, 1e20])
    parser.add_argument('-a','--activity', help='choose between Velo, Tracker; default is Velo', choices = ['Velo', 'Tracker'] ,default= 'Velo')
    parser.add_argument('--AligWork_dir', help='folder with alignments', default = std_AligWork_dir)
    parser.add_argument('--alignment_dir', help='folder with alignments updated', default = std_alignment_dir)
    parser.add_argument('--references', help='file with references file', default = std_references)
    parser.add_argument('-f', '--trueUpdate', help='Check the updates comparing constants', action='store_true')
    parser.add_argument('--reload', help='Make again pickle file even if it is already there', action='store_true')
    args = parser.parse_args()
##########################

if __name__ == '__main__':

    import pickle
    activity = args.activity

    if not os.path.exists('{0}.pkl'.format(activity)) or args.reload:
        from trendPlots import getListRuns, getListRunsUpdated, getListRunsTrueUpdate

        runs = getListRuns(args.activity, args.AligWork_dir)
        runs = [i for i in runs if i >= args.runs[0] and i <= args.runs[1]]

        if args.trueUpdate:
            from computeTrueUpdate import isUpdate
            runsUpdated = getListRunsTrueUpdate(runs, args.activity, args.alignment_dir, args.AligWork_dir)
        else:
            runsUpdated = getListRunsUpdated(args.activity, args.alignment_dir)
            runsUpdated = [i for i in runsUpdated if i in runs]

        print 'Considering {0} runs'.format(len(runs))
        updatesRuns = [runs[0]]

        n_runs = len(runs)
        n_updates = len(runsUpdated)

        xml_path = os.path.join(args.AligWork_dir,'{activity}/run{run}/xml/{detector}/{part}.xml')

        updates = [(n_updates, n_runs)]
        

        from GaudiPython import gbl
        AlMon = gbl.Alignment.AlignmentMonitoring
        from computeTrueUpdate import std_vector, isUpdate

        if args.activity == 'Velo':
            detectors = {'Velo' : ['VeloGlobal', 'VeloModules']}
        elif args.activity == 'Tracker':
            detectors = {'TT' : ['TTGlobal', 'TTModules'],
                        'IT' : ['ITGlobal', 'ITModules'],
                        'OT' : ['OTGlobal', 'OTModules'],}        
            # detectors = {'IT' : ['ITGlobal', 'ITModules']}
        elif args.activity == 'Muon':
            detectors = {'Muon' : ['MuonGlobal', 'MuonModules']}


        for run in runsUpdated:
            cc = AlMon.CompareConstants()
            cc.Compare(std_vector([xml_path.format(activity=activity,detector = detector, part=part, run = updatesRuns[-1]) for detector in detectors for part in detectors[detector]]),
                           std_vector([xml_path.format(activity=activity,detector = detector, part=part, run = run) for detector in detectors for part in detectors[detector]]))
            flag, alignables_update = isUpdate(cc, activity)
            print "Run %d Flag %s. Activity %s." % (run, flag, alignables_update) 
            if flag:
                updatesRuns.append(run)
                updates.append((run,alignables_update))
                


        pickle.dump(updates, open('{0}.pkl'.format(activity), 'wb'))

        
    # Now I have the list of updates

    list_updates = pickle.load(open('{0}.pkl'.format(activity), 'rb'))
    n_updates, n_runs = list_updates[0]
    list_updates = list_updates[1:]

    dofs_update = {}

    for update in list_updates:
        dofs = tuple(sorted([i for i in update[1].split(';') if i]))
        if not dofs_update.has_key(dofs):
            dofs_update[dofs] = []
        dofs_update[dofs].append(update[0])


    ll = sorted(dofs_update.items(), key = lambda x: len(x[1]), reverse = True)
    # print dofs_update
    print ll
    print
    print '{0} updates in {1} runs (ratio = {2:.2f})'.format(n_updates, n_runs, float(n_updates)/n_runs)
    print
    print [(key, len(value)) for key, value in ll]

    if activity == 'Velo':
        updatesForComb = [(tuple([i.replace('VeloLeft.','') for i in key]), len(value)) for key, value in ll]
        updatesForDof = [(dof, sum([i[1] for i in updatesForComb if dof in i[0]])) for dof in ['Tx','Ty','Tz','Rx','Ry','Rz']]
        
        
        import matplotlib.pyplot as plt
        import matplotlib

        plt.figure(figsize=(8,8))
        plt.axes([0.1, 0.15, 0.7, 0.7])
        
        plt.pie([i[1] for i in updatesForComb], labels=[i[0] for i in updatesForComb],
                    autopct='%1.1f%%',labeldistance=1.2, pctdistance=1.1)
        plt.savefig('updatesForComb.pdf')

        font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : 18}

        matplotlib.rc('font', **font)
        
        plt.figure(figsize=(8,8))
        plt.axes([0.05, 0.05, 0.9, 0.9])
        
        plt.pie([i[1] for i in updatesForDof], labels=[i[0] for i in updatesForDof],
                    autopct='%1.1f%%',labeldistance=1.2, pctdistance=0.85)
        plt.savefig('updatesForDof.pdf')

        
