#!/usr/bin/env python

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description ="Macro to make root file with convergence plots from alignlog.txt")
    parser.add_argument('-r','--run', help='run number, default is the last one')
    parser.add_argument('--alignlog', help='location of alignlog.txt, default is guessed by run number')
    parser.add_argument('-a','--activity', help='choose between Velo, Tracker, Muon; default is Velo', choices = ['Velo', 'Tracker', 'Muon'] ,default= 'Velo')
    parser.add_argument('-o','--outFile',help='output file name')
    args = parser.parse_args()

##########################

import os, sys, fnmatch, re, time
import ROOT as r
from GaudiPython import gbl
from AlignmentOutputParser.AlignOutput import *
from AlignmentMonitoring.OnlineUtils import findLastRun, findAlignlog, findHistos
from AlignmentMonitoring.RootUtils import makeGraphHisto, getExpression, getDofDeltaConvergence

AlMon = gbl.Alignment.AlignmentMonitoring


def runFromAlignlog(alignlog):
    '''
    Get first run number analised from alignlog
    '''
    text = open(alignlog).read()
    try:
        run = re.findall('First/last run number: ([0-9]*)/', text)[0]
    except IndexError:
        raise IOError('run number not present in alignlog '+alignlog)
    return run
    

def alignlog2root(alignlog, outFile_name, openString='recreate'):
    '''
    Read alignlog file and write histograms with trend plots vs iterations
    in root file
    optionString can be "recreate" or "update" depending if in the file there is
    already something that should not be delated
    '''

    # read alignlog
    aout = AlignOutput(alignlog)
    aout.Parse()

    # Open output root file
    outFile = r.TFile(outFile_name, openString)
    outDir = outFile.mkdir('alignlog', 'alignlog')
    outDir.cd()

    # Histograms Convergence chi2 etc
    for expression, title in [('TrChi2nDof', 'Track #chi^{2}/dof'), ('VChi2nDof', 'Vertex #chi^{2}/dof'), ('NormalisedChi2Change', '#Delta #chi^{2}/dof')]:
        histo = makeGraphHisto(expression, '{0};Iterations;{0}'.format(title), getExpression(aout, expression))
        histo.Write()

    # Histograms Convergence DOFs
    for alignable in aout.AlignIterations[0].Alignables.keys():
        for dof in [i.Name for i in aout.AlignIterations[0].Alignables[alignable].AlignmentDOFs]:
            unit = '#mum' if 'T' in dof else '#murad'
            histo = makeGraphHisto('{0}_{1}'.format(alignable, dof),'{0};Iterations;#Delta{1} [{2}]'.format(alignable, dof, unit), *getDofDeltaConvergence(aout, dof=dof, alignable=alignable))
            histo.Write()
        
    outFile.Write()
    outFile.Close()

if __name__ == '__main__':

    if not args.alignlog:
        run = args.run if args.run else findLastRun(args.activity)
    alignlog = args.alignlog if args.alignlog else findAlignlog(args.activity, run)

    if args.run == None:
        run = runFromAlignlog(alignlog)

    # print alignlog

    alignlog_dir, alignlog_file = os.path.split(alignlog)
    outFile_name = args.outFile if args.outFile else os.path.join(alignlog_dir, 'AligHistos{0}_run{1}_{2}.root'.format(args.activity, run, time.strftime('%Y%m%dT%H%M%S')))
    
    alignlog2root(alignlog, outFile_name)


