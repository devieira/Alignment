// From Gaudi
#include "GaudiAlg/GaudiTool.h"

// Interface
#include "AlignmentInterfaces/IWriteAlignmentConditionsTool.h"

// From Det
#include "DetDesc/TabulatedProperty.h"

// From Rich
#include "RichDet/DeRichSystem.h"

// std
#include <sstream>
#include <fstream>
#include <streambuf>
#include <boost/filesystem.hpp>

// All HPD QEs for Rich1 and Rich2 are in Rich1/Environment/HpdQuantumEfficiencies.xml

class WriteRichHPDQEsTool
  : public GaudiTool, virtual public IWriteAlignmentConditionsTool
{
public:
  /// constructor
  WriteRichHPDQEsTool( const std::string& type,
                       const std::string& name,
                       const IInterface* parent); ///< Standard constructor

  /// destructor
  ~WriteRichHPDQEsTool();

  /// initialize
  StatusCode initialize() override;

  // Everything configured via options
  StatusCode write() const override;

  // Everything configured via options
  StatusCode write(const std::string& version) const override;

private:
  StatusCode createXmlFile(const std::string& filename,
                           const std::string& contents,
                           const std::string& version) const;

  StatusCode writeHPDQEs(const std::string& version) const;
// StatusCode writeHPDQEsFromDB(const std::string& version) const;

  /// Pointer to Rich Sys Detector Element
  const DeRichSystem * m_RichSys = nullptr;

  std::string m_directory;
  unsigned int m_precision;
  std::string m_author;
  std::string m_description;
  std::string m_version;
  bool m_online;

};


/******************************************************************************/
/* Implementation                                                             */
/******************************************************************************/

DECLARE_COMPONENT( WriteRichHPDQEsTool )

WriteRichHPDQEsTool::WriteRichHPDQEsTool( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent)
: GaudiTool( type,name,parent)
{
  // interface
  declareInterface<IWriteAlignmentConditionsTool>(this);
  // properties
  declareProperty("Directory", m_directory = "xml" );
  declareProperty("Precision", m_precision = 8u);
  declareProperty("Author", m_author = "");
  declareProperty("Version", m_version = "");
  declareProperty("Description", m_description = "");
  declareProperty("OnlineMode", m_online = false);
}

WriteRichHPDQEsTool::~WriteRichHPDQEsTool() {}

StatusCode WriteRichHPDQEsTool::initialize()
{
  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  /// Get pointer to detector
  m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  return StatusCode::SUCCESS;
}


StatusCode WriteRichHPDQEsTool::write() const
{
  return write(m_version);
}


StatusCode WriteRichHPDQEsTool::write(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

  sc = writeHPDQEs(version);
  // sc = writeHPDQEsFromDB(version); // We don't use this

  if (sc.isFailure() ) return Warning( "Failed to write tabulated properties to xml", StatusCode::FAILURE );
  return sc;
}
/* COMMENTED OUT DUE TO NOT UNDERSTANDING TABULATED PROPERTY YET
namespace {
  void stringreplace(std::string& astring,
                     const std::string& in,
                     const std::string& out,
                     size_t pos=0)
  {
    while( pos < astring.size() && (pos = astring.find(in,pos))!=std::string::npos) {
      astring.replace( pos, in.size(), out);
      pos += out.size();
    }
  }


  std::string formattedtabprop( const TabulatedProperty& tabprop, unsigned int precision )
  {
    std::ostringstream strstream;
    strstream << tabprop.toXml( "", false , precision ) << std::endl;
    std::string rc = strstream.str();

    stringreplace(rc,"tabproperty>","tabproperty>\n");

    return rc;
  }
}
*/

StatusCode WriteRichHPDQEsTool::createXmlFile(const std::string& filename,
                                              const std::string& /*version*/,
                                              const std::string& contents) const
{
  /// create directory if it does not exist
  boost::filesystem::path apath(filename);
  boost::filesystem::create_directories(apath.parent_path());

  /// create the output file
  std::ofstream output( filename );
  if ( output.fail() ) {
    return Warning( "Failed to open output file " + filename, StatusCode::FAILURE );
  }

  // write the header
  if (!m_online) {
    output  << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
            << "<!DOCTYPE DDDB SYSTEM \"conddb:/DTD/structure.dtd\">\n";
    output  << "<DDDB>\n\n";
  }

  /// write the contents
  output << contents;

  /// write the footer
  if (!m_online) {
    output << "</DDDB>\n";
  }
  /// close the file
  output.close();

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Write HPD QEs
//=============================================================================
StatusCode WriteRichHPDQEsTool::writeHPDQEs(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

  std::string infile_start = "/group/online/alignment/Rich1/HpdQE/v";
  std::string infile_end = ".xml";
  int number = 0;
  std::stringstream infile;
  while (1 == 1)
  {
	std::stringstream test;
    test << infile_start << (number + 1) << infile_end;
    if (boost::filesystem::exists(test.str()) == false) break;
	number++;
  }
  infile << infile_start << number << infile_end;

  std::string file = "Rich1/Environment/HpdQuantumEfficiencies.xml";

  std::stringstream RichQEs;

  std::ifstream t(infile.str());
  std::string str;

  t.seekg(0, std::ios::end);
  str.reserve(t.tellg());
  t.seekg(0, std::ios::beg);

  str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

  RichQEs << str ;

  std::string fileName;
  fileName = m_directory + "/" + file;
  sc = createXmlFile( fileName, version, RichQEs.str() );

  return sc;
}

/*
//====================================================================================================
//  Write HPD QEs from DB [We do not use this function! allPDHardwareIDs() does not include past HIDs]
//  Also "TabulatedProperty" does not work like "Condition" so if we ever have to deal with this we
//  have to figure this out
//====================================================================================================
StatusCode WriteRichHPDQEsTool::writeHPDQEsFromDB(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

  std::string file = "Rich1/Environment/HpdQuantumEfficiencies.xml";

  std::stringstream RichQEs;

  // allPDHardwareIDs() should return a list of all (active and inactive) PDs identified by their hardware IDs
  const Rich::DAQ::HPDHardwareIDs HIDs = m_RichSys->allPDHardwareIDs() ;

  // ==========================================
  // Loop over HPD Hardware IDs
  // ==========================================
  for( Rich::DAQ::HPDHardwareIDs<Rich::DAQ::HPDHardwareID>::const_iterator it = HIDs.begin() ; it != HIDs.end() ; ++it )
  {
	// Get tabulated property
	if ( msgLevel(MSG::DEBUG) ) debug() << "HPD Hardware ID " << (std::string)*it << endmsg;
    std::string QEsLoc = "/dd/Conditions/Environment/Rich1/QuantumEffHpd_H"+(std::string)*it;
    TabulatedProperty *myTabProp = get<TabulatedProperty>( detSvc(), QEsLoc );
    RichQEs << formattedtabprop( *myTabProp, m_precision );
  } // Loop over HPD Hardware IDs

  std::string fileName;
  fileName = m_directory + "/" + file;
  sc = createXmlFile( fileName, version, RichQEs.str() );

  return sc;
}
*/
