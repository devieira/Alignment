#ifndef DToKPiTwoProng_H_
#define DToKPiTwoProng_H_

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Event/Track.h"


namespace LHCb{

  class Particle;
  class TwoProngVertex;

}

class ILHCbMagnetSvc;
class IHitExpectation;
class ITrackVertexer;
class ITrackExtrapolator;

class ITrackFitter;


class DToKPiTwoProng final : public GaudiTupleAlg{

 public:

  DToKPiTwoProng(const std::string& name, ISvcLocator* pSvc);

  StatusCode initialize() override;
  
  StatusCode execute() override;

  
 private:

  LHCb::Track* track(const LHCb::Particle* part) const;


  std::unique_ptr<LHCb::TwoProngVertex> refittedMass(const LHCb::Track& track1, 
                               const LHCb::Track& track2,
                               double zVert ) const;

  double PIDK(const LHCb::Particle* part) const;


  ITrackVertexer* m_vertexer;
  ITrackExtrapolator* m_trackExtrapolator;

  ITrackFitter* m_trackFit;
  ITrackFitter* m_trackPreFit;

  std::string m_particleLocation;
  std::string m_vertexLocation;
  std::string m_trackOutputLocation;
  std::string m_resonanceName;

  double m_pionMass;
  double m_kaonMass;
  double m_deltaMass;
  double m_minMass;
  double m_maxMass;


};

#endif
