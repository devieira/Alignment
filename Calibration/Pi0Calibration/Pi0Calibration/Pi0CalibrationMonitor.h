/*
 * =====================================================================================
 *
 *       Filename:  Pi0CalibrationMonitor.h
 *
 *    Description:  main algorithm for pi0 calibration at LHCb online
 *
 *        Version:  1.0
 *        Created:  11/21/2016 01:47:23 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef CALIBRATION_PI0CALIBRATION_PI0CALIBRATIONMONITOR_H
#define CALIBRATION_PI0CALIBRATION_PI0CALIBRATIONMONITOR_H

#include <string>
#include <map>
#include <vector>
#include <utility>

#include "GaudiKernel/Point3DTypes.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/CaloCellID.h"

#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "Math/SMatrix.h"

#include "Pi0Calibration/Pi0LambdaMap.h"


class Pi0CalibrationMonitor : public GaudiAlgorithm {


    public:
	Pi0CalibrationMonitor( const std::string& name, ISvcLocator* pSvcLocator);
	virtual ~Pi0CalibrationMonitor();

        virtual StatusCode initialize() override;
        virtual StatusCode execute() override;
        virtual StatusCode finalize() override;

	void plots();

    private:
	std::string m_tupleFileName;
        std::string m_tupleName;
        std::string m_outputDir;
        std::string m_inputDir;

        double m_pdgPi0;

        std::vector<unsigned int > m_indices;
        std::vector<LHCb::CaloCellID> m_cells;

	std::map<std::string, std::map<unsigned int, Gaudi::XYZPoint > > m_map;
	std::map<std::string, double> m_cellsize;
  
	std::vector<LHCb::CaloCellID> m_skippedCells;
	void readCells( std::vector<LHCb::CaloCellID>& cells);
	void readValues( std::string varname, std::map<unsigned int, std::pair<double,double> >& values );
	void monitorPlots( std::string name, double centreVal, const std::map<unsigned int, double>& values );
	void monitorPlots( std::string name, double centreVal, const std::map<unsigned int, std::pair<double,double> >& values );

};

#endif
