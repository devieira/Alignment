/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFiller.h
 *
 *    Description:  (1) create the pi0 mass histogram; 
 *                  (2) fit the histogram; 
 *                  (3) save and return the results
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:32:16 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef CALIBRATION_PI0CALIBRATION_PI0MASSFILLER_H
#define CALIBRATION_PI0CALIBRATION_PI0MASSFILLER_H 1

#include <string>
#include <map>
#include <vector>

#include "TH1.h"
#include "TH2.h"

#include "Pi0Calibration/Pi0LambdaMap.h"
#include "Pi0Calibration/MMapVector.h"
#include "Pi0Calibration/Pi0CalibrationFile.h"

#include "Kernel/CaloCellID.h"

const unsigned int BitsCol     = 6 ;
const unsigned int BitsRow     = 6 ;
const unsigned int BitsArea    = 2 ;
const unsigned int ShiftCol    = 0 ;
const unsigned int ShiftRow    = ShiftCol  + BitsCol  ;
const unsigned int ShiftArea   = ShiftRow  + BitsRow  ;
const unsigned int MaskArea = ( ( ( (unsigned int) 1 ) << BitsArea ) - 1  ) << ShiftArea ;

namespace Calibration
{
    namespace Pi0Calibration 
    {
	class Pi0MassFiller
	{
	    public:
		Pi0MassFiller();
		Pi0MassFiller( const std::string& filename );
		Pi0MassFiller( const std::string& filename, const std::string& tuplename, const std::vector<unsigned int>& indices, const Calibration::Pi0Calibration::Pi0LambdaMap& lambdamap, const std::vector<double>* vecBeta, const std::vector<double>* vecGamma );
		Pi0MassFiller( const std::string& filename, const std::vector<unsigned int>& indices, const Calibration::Pi0Calibration::Pi0LambdaMap& lambdamap, const std::vector<double>* vecBeta, const std::vector<double>* vecGamma );

		virtual ~Pi0MassFiller(){ m_hists->Clear(); }//---m_hists_bg->Clear(); }

                TH2* hists() { return m_hists; }

		static double scale_factor( Candidate c, const Pi0LambdaMap& lambdamap, const std::vector<double>* vecBeta, const std::vector<double>* vecGamma ){
                  auto ind1 = c.ind1;
                  auto ind2 = c.ind2;
                  auto prs1 = c.prs1;
                  auto prs2 = c.prs2;
                  auto g1E  = c.g1E;
                  auto g2E  = c.g2E;
                  double scale = 1.;
                  std::pair<double,double> lambda1 = lambdamap.get_lambda( ind1 );
                  std::pair<double,double> lambda2 = lambdamap.get_lambda( ind2 );
                  if( lambda1.first<0.0 ) lambda1 = std::make_pair( 1.0, 0.0);
                  if( lambda2.first<0.0 ) lambda2 = std::make_pair( 1.0, 0.0);

		  const unsigned int area1 = ( uint(ind1) & MaskArea  ) >> ShiftArea; //0: outer, 1: middle, 2: inner
		  const unsigned int area2 = ( uint(ind1) & MaskArea  ) >> ShiftArea; //0: outer, 1: middle, 2: inner

		  double gamma1 = vecGamma->at(area1+2);
		  double gamma2 = vecGamma->at(area2+2);
		  double beta1 = vecBeta->at(area1+2);
		  double beta2 = vecBeta->at(area2+2);
		 
		  scale = std::sqrt(lambda1.first+(1.0-lambda1.first)*(beta1*prs1+gamma1)/g1E)
		    *std::sqrt(lambda2.first+(1.0-lambda2.first)*(beta2*prs2+gamma2)/g2E);

                  return scale;
                }

	    private:
		TH2* m_hists;

	};
    }//namespace Pi0Calibration
}//namespace Calibration

#endif
