#--> perform an iteration of the pi0 calibration: fill the histogram, perform the fit and extract the calibration constants.
#--> in default, only bad fits and cells with low statistics are saved; if VERBOSE invoked, all the fits are saved on disk.
#--> three types of input files are allowed: ROOT (TTree), MMap, ROOT (TH2D: names hists & hists_bg)
#--> any further questions, please contact Zhirui at zhirui@cern.ch

import os
import argparse
parser = argparse.ArgumentParser( description = "do_calibration" )
parser.add_argument( "--filename",  type=str, dest="filename",  default=None )
parser.add_argument( "--tuplename", type=str, dest="tuplename", default="KaliPi0/Pi0-Tuple")
parser.add_argument( "--iteration", type=int, dest="iteration")
parser.add_argument( "--outdir",    type=str, dest="outdir")
parser.add_argument( "--indir",     type=str, dest="indir")
parser.add_argument( "--year",      type=str, dest="year")
parser.add_argument( "--filetype",  type=str, dest="filetype", help="read from the {MMap,ROOT,TH2D} file")


args = parser.parse_args()
tuplefile = args.filename
tuplename = args.tuplename
iteration = args.iteration
year      = args.year
outputdir = args.outdir
inputdir  = args.indir
filetype  = args.filetype

from Gaudi.Configuration import *
from Configurables import LHCbApp
LHCbApp().DataType = year
LHCbApp().EvtMax   = -1
#LHCbApp().OutputLevel = VERBOSE

from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc
CondDB().UseOracle = False
CondDB().UseLatestTags = [ year ]

#import time
#suffix = time.strftime("%Y%m%d-%H:%M:%SGMT", time.gmtime())

from GaudiPython import *
ApplicationMgr( OutputLevel=INFO, AppName="Pi0Calibration" )

from Configurables import Pi0CalibrationAlg
pi0Calib = Pi0CalibrationAlg("Pi0Calibration")
pi0Calib.OutputLevel   = ERROR #VERBOSE: save all fits
pi0Calib.tupleFileName = tuplefile
pi0Calib.tupleName     = tuplename
pi0Calib.filetype      = filetype
pi0Calib.outputDir     = os.path.join( outputdir, str(iteration) )
pi0Calib.lambdaFileName= os.path.join( inputdir, str(iteration-1), "lambda.txt" ) 
pi0Calib.saveLambdaFile= os.path.join( outputdir, str(iteration), "lambda.txt" )

from Configurables import Pi0CalibrationMonitor
pi0Moni = Pi0CalibrationMonitor("Pi0CalibrationMonitor")
pi0Moni.tupleFileName = tuplefile
pi0Moni.tupleName     = tuplename
pi0Moni.outputDir     = os.path.join( outputdir, str(iteration) )
pi0Moni.inputDir      = os.path.join( outputdir, str(iteration) )


from Configurables import GaudiSequencer
mainSeq = GaudiSequencer("MainSeq")
mainSeq.Members = [ pi0Calib, pi0Moni ]
ApplicationMgr().TopAlg.append( mainSeq )
AppMgr().run( 1 )

