#!/bin/python

#--> convert the ROOT (TTree) to MMap file
#--> any further questions, please contact Zhirui at zhirui.xu@cern.ch

from GaudiPython import gbl
pi0Calib = gbl.Calibration.Pi0Calibration

import os
dataName  = "Second" 
tuplefile = "/gpfs/LAPP-DATA/lhcb/xu/%s_data_2016_pi0.root"%dataName
tuplename = "KaliPi0/Pi0-Tuple"
outputfile = "/gpfs/LAPP-DATA/lhcb/xu/%s_data_2016_pi0.MMap"%dataName

if '__main__' == __name__ :
    pi0Calib.Pi0CalibrationFile( tuplefile, tuplename, outputfile )
