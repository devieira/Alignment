/*
 * =====================================================================================
 *
 *       Filename:  Pi0MMap2Histo.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/27/2017 05:05:55 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>
#include <cstdint>

#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticlePropertySvc.h"

#include "CaloDet/DeCalorimeter.h"
#include "Kernel/CaloCellCode.h"
#include "Kernel/CaloCellID.h"
#include "CaloKernel/CaloVector.h"
#include "CaloDet/CellParam.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiAlg/GaudiAlg.h"

#include "TFile.h"
#include "TH2.h"
#include "TTree.h"

//from local
#include "Pi0Calibration/MMapVector.h"
#include "Pi0Calibration/Pi0CalibrationFile.h"
#include "Pi0Calibration/Pi0LambdaMap.h"
#include "Pi0Calibration/Pi0MMap2Histo.h"
#include "Pi0Calibration/Pi0MassFiller.h"
//from boost
#include <boost/filesystem.hpp>

#include "DetDesc/Condition.h"

using namespace boost;
using namespace boost::filesystem;
using namespace Calibration::Pi0Calibration;

DECLARE_COMPONENT( Pi0MMap2Histo )

Pi0MMap2Histo::Pi0MMap2Histo( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ){

  declareProperty(     "filenames", m_filenames  );
  declareProperty(     "outputDir", m_outputDir  );
  declareProperty(    "outputName", m_outputName );
  declareProperty(       "nworker", m_nworker    );
  declareProperty("lambdaFileName", m_lambdaFileName="");

}

Pi0MMap2Histo::~Pi0MMap2Histo(){}

StatusCode Pi0MMap2Histo::initialize(){
  
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() ) return sc;

  // import a list of calo cells
  DeCalorimeter* m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  const CaloVector<CellParam>& cells = m_calo->cellParams();
  for( CaloVector<CellParam>::const_iterator icell = cells.begin(); icell != cells.end(); icell++ ){
    LHCb::CaloCellID id = (*icell).cellID();
    if( !m_calo->valid( id ) || id.isPin() ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::OfflineMask) ) continue;
    m_indices.push_back( id.index() );
  }

  // Get beta and gamma parameters from the DB, for ECorrections
  registerCondition("Conditions/Reco/Calo/PhotonECorrection", m_myCond, &Pi0MMap2Histo::cacheConditionData);
  sc = runUpdate();
  if (sc.isFailure()) {
    return Error("Could not update conditions from the CondDB.", sc);
  }

  return sc;
}

StatusCode Pi0MMap2Histo::execute(){
  StatusCode sc = StatusCode::SUCCESS;

  return sc;
}

StatusCode Pi0MMap2Histo::finalize(){
  StatusCode sc = StatusCode::SUCCESS;

  info() << "Pi0MMap2Histo : START  " << endmsg;

  std::vector< std::vector<unsigned int> > subtasks( m_nworker );
  for( auto& c:subtasks ) c.reserve( m_indices.size()/m_nworker );
  info() << "total cells to be processed by each worker " << m_indices.size()/m_nworker << endmsg;

  int i=0;
  for( auto index : m_indices ) subtasks[i++%m_nworker].push_back( index );

  auto  min_max = std::minmax_element( m_indices.begin(), m_indices.end() );
  int xmax = min_max.second-m_indices.begin();

  Pi0LambdaMap lambdamap( m_indices );
  if( exists(m_lambdaFileName) ) lambdamap.loadMapFromFile( m_lambdaFileName );
  if(msgLevel(MSG::INFO)) info() << " the lambda is loaded from file " << m_lambdaFileName << endmsg;

  int iwoker = 1;
  for( auto& subtask : subtasks ){
    info() << " start worker " << iwoker << " for the following indices: "<< endmsg;

    auto sel = Pi0CalibrationFile::make_indices_selector( subtask );
    info() << "make_indices_selector called successfully " << endmsg;

    info() << " a list of file names " << endmsg;
    for( auto& filename : m_filenames ) info() << "  file : " << filename << endmsg;

    boost::filesystem::path outfilename( m_outputDir+"/"+m_outputName+"_Worker"+std::to_string(iwoker)+".root" );
    TFile *fout = new TFile( outfilename.c_str(), "RECREATE" );
    TH2D *hists    = new TH2D("hists",    "hists",    m_indices.at(xmax), 0, m_indices.at(xmax), 100, 0.0, 250.0);

    for( auto& filename : m_filenames ){
      if( !exists(filename) ){ 
	err() << "file not exist! "<< filename << endmsg; 
	continue;
      }
      info() << "start to read file from " << filename << endmsg;
      MMapVector<Candidate> vorigin(filename.c_str(), MMapVector<Candidate>::ReadOnly);
      info() << "open the file successfully with " << vorigin.size() << " entries " << endmsg;
      for( auto& c : vorigin ){
        auto bkg  = c.bkg; 
        auto ind1 = c.ind1;
        auto ind2 = c.ind2;
        auto m12  = c.m12;

	auto scale = Pi0MassFiller::scale_factor(c, lambdamap, m_beta, m_gamma);
	//select the cells to fill in the histograms
	if( sel(ind1) ){ 
          if(bkg==0) hists->Fill( ind1, m12*scale );
	}
	if( sel(ind2) ){ 
          if(bkg==0) hists->Fill( ind2, m12*scale );
	}
      }
      info() << "finished reading file " << filename << endmsg;
    }// all input files
    hists->Write();
    fout->Close();

    info() << "saved histograms for worker " << iwoker << endmsg;

    iwoker++;
  }// all the workers

  return sc;
}

StatusCode Pi0MMap2Histo::cacheConditionData() {

  if( m_myCond -> exists( "beta" ) )
    m_beta = &(m_myCond->paramAsDoubleVect("beta"));

  if( m_myCond -> exists( "offset" ) )
    m_gamma = &(m_myCond->paramAsDoubleVect("offset"));
 
  return StatusCode::SUCCESS;
}

