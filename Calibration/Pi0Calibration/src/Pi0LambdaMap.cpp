/*
 * =====================================================================================
 *
 *       Filename:  Pi0LambdaMap.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/06/2016 02:34:56 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (LAPP-CNRS), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */


#include <iostream>
#include <fstream>
//from local
#include "Pi0Calibration/Pi0LambdaMap.h"
//from ROOT
#include "TFile.h"
//from boost
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp> 
#include "boost/regex.hpp"
#include <ctime>

using namespace boost;
using namespace boost::filesystem;
using namespace Calibration::Pi0Calibration;

Pi0LambdaMap::Pi0LambdaMap(const std::vector<unsigned int>& indices) {  
  for(auto index : indices ) m_lambdas[index] = std::make_pair( -1., -1.);
}

void Pi0LambdaMap::loadMapFromFile( const std::string& filename ){
  if( !exists(filename) ) return;
  
  std::string line;
  std::ifstream ifile( filename );
  if( ifile.is_open() ){
    while( std::getline(ifile,line) ){
      std::vector<std::string> values;
      values.clear();
      boost::split( values, line, boost::is_any_of("\t "), boost::token_compress_on );
      if( values.size()!=3 || values[0].find("#")!=std::string::npos ) continue;
      if( values.size()==3 ) {
        m_lambdas[ std::stoul(values[0]) ] = std::make_pair( std::stod(values[1]), std::stod(values[2]) );
	//std::cout << " string to unsigned integer : " << values[0] << " = " << std::stoul( values[0] ) << std::endl;
	//std::cout << " string to double           : " << values[1] << " = " << std::stod( values[1] ) << std::endl;
	//std::cout << " string to double           : " << values[2] << " = " << std::stod( values[2] ) << std::endl;
      }
    }
    ifile.close();
  }
   
}

void Pi0LambdaMap::setLambda( const unsigned int index, const std::pair<double,double>& lambda ){
  m_lambdas[index] = lambda;
}

std::pair<double,double> Pi0LambdaMap::get_lambda(const unsigned int index ) const {
  //if( m_lambdas.find(index)!=m_lambdas.end() ) return m_lambdas[index];
  //else return std::make_pair(1.0,0.0);
  auto it = m_lambdas.find(index);
  //return (m_lambdas.end()==it || it->second.first<0) ? std::make_pair(1.0,0.0) : it->second;
  return (m_lambdas.end()==it) ? std::make_pair(-1.,-1.) : it->second;
}

void Pi0LambdaMap::saveMapToFile( const std::string& filename ){
  std::ofstream ofile;
  ofile.open( filename , std::ios::out | std::ios::trunc );
  for( auto value : m_lambdas ){
    //std::cout << value.first << "  " << value.second.first << "  +/-  " << value.second.second << std::endl;
    if( value.second.first < 0.0 || value.second.second < 0.0) continue;
    ofile << value.first << "  " << value.second.first << "  " << value.second.second << "\n";
  }
  ofile.close();
}

void Pi0LambdaMap::saveMapToDBFile( const std::string& filename ){
  time_t now = time(0);
  char* dt = ctime(&now);
  std::ofstream ofile;
  ofile.open( filename , std::ios::out | std::ios::trunc );
  ofile << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> \n<!--- $Id: -->\n<!-- Jean-Francois Marchand -->\n<!--- Created : " << dt << " -->\n<!--- condDB for data -->\n<!DOCTYPE DDDB SYSTEM \"../../../DTD/structure.dtd\">\n<DDDB>\n <condition name = \"Calibration\">\n <param name = \"size\"       type = \"int\">   2 </param>\n    <paramVector name = \"data\" type = \"double\">\n      <!---   channel   dG  -->\n";
  for( auto value : m_lambdas ){
    //std::cout << value.first << "  " << value.second.first << "  +/-  " << value.second.second << std::endl;
    if( value.second.first < 0.0 || value.second.second < 0.0) continue;
    if (value.second.first>0.6 && value.second.first<1.4) 
      ofile << value.first << "  " << value.second.first << "\n";
  }
  ofile << "    </paramVector>\n</condition>\n</DDDB>\n";
  ofile.close();
}
