/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFitter.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:40:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */


//from ROOT
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TSpectrum.h"
#include "TF1.h"
#include "TMath.h"
#include "TStyle.h"
#include "TError.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
//from RooFit
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooRealVar.h"
#include "RooChebychev.h"
#include "RooGaussian.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooFormulaVar.h"
#include "RooGenericPdf.h"
#include "RooProdPdf.h"

//from local
#include "Pi0Calibration/Pi0MassFitter.h"

//from boost
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace RooFit;
using namespace Calibration::Pi0Calibration;

const unsigned int BitsCol     = 6 ;
const unsigned int BitsRow     = 6 ;
const unsigned int BitsArea    = 2 ;
const unsigned int ShiftCol    = 0 ;
const unsigned int ShiftRow    = ShiftCol  + BitsCol  ;
const unsigned int ShiftArea   = ShiftRow  + BitsRow  ;
const unsigned int MaskArea = ( ( ( (unsigned int) 1 ) << BitsArea ) - 1  ) << ShiftArea ;


Double_t Pi0MassFitter::polyn(Double_t *x, Double_t *par) {
  return par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
}
Double_t Pi0MassFitter::polynSB(Double_t *x, Double_t *par) {
  if (x[0] > 90 && x[0] < 170) {
    TF1::RejectPoint();
    return 0;
  }
  return par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
}
Double_t Pi0MassFitter::signal(Double_t *x, Double_t *par) {
  return par[0] * exp(-0.5 * pow(((x[0]-par[1])/par[2]),2) );
}
Double_t Pi0MassFitter::fitFunctionPol(Double_t *x, Double_t *par) {
  return polyn(x,par) + signal(x,&par[3]);
}



Pi0MassFitter::Pi0MassFitter(TH1* hist, double mpi0, double sigma){
  m_hist    = hist;
  m_mpi0    = mpi0;
  m_sigma0 = sigma;

  m_mean    = std::make_pair(0.,0.);
  m_sigma   = std::make_pair(0.,0.);
  m_nsignal = std::make_pair(0.,0.);
  m_status= -1;
}

void Pi0MassFitter::chi2fit( const std::string& outputdir, const std::string& outfilename, bool verbose){
  std::string subdir = "good";

  gErrorIgnoreLevel = kError;

  std::string hist_name = m_hist->GetName();
  unsigned int cellID = (unsigned int) atoi((hist_name.substr(hist_name.find("_ID")+3,hist_name.length()).substr(0,hist_name.substr(hist_name.find("_ID")+3,hist_name.length()).find("_"))).c_str());

  if( m_hist->GetEntries() < 2500 || cellID==855 ){
    subdir = "nofit";

    gStyle->SetOptFit(1112);
    gStyle->SetStatFormat("6.2g");
    gStyle->SetStatStyle(4001);
    TCanvas* cc = new TCanvas();
    m_hist->Draw();

    if( !exists(outputdir) ) create_directories( outputdir );
    if( !exists(outputdir+"/"+subdir) ) create_directories( outputdir+"/"+subdir );
    boost::filesystem::path outfile(outputdir+"/"+subdir+"/"+outfilename);

    cc->SaveAs( outfile.c_str() );
    cc->Close();
    return;
  }


  TF1* fitBkg = new TF1("fitBkg",polynSB,75,205,3);
  m_hist->Fit("fitBkg","LQ","",80,195);
  double par0 = fitBkg->GetParameter(0);
  double par1 = fitBkg->GetParameter(1);
  double par2 = fitBkg->GetParameter(2);
  TF1* model = new TF1("model",fitFunctionPol,75,205,6);
  model->SetParameters(par0, par1, par2, 1000, 133, 8);
  model->SetParLimits(3,1,10000);
  model->SetParLimits(4,115,170);
  model->SetParLimits(5,4,25);
  TFitResultPtr fitres = m_hist->Fit("model","LQS","",75,205);

  bool isGoodFit = false;

  if ( !( (model->GetParameter(5)>4.1 && model->GetParameter(5)<24.9) &&
	  (model->GetParameter(4)>116 && model->GetParameter(4)<169) ) //||        
       ) {

    TF1* fitBkg = new TF1("fitBkg",polynSB,80,205,3);
    m_hist->Fit("fitBkg","LQ","",80,210);
    double par0 = fitBkg->GetParameter(0);
    double par1 = fitBkg->GetParameter(1);
    double par2 = fitBkg->GetParameter(2);
    model = new TF1("model",fitFunctionPol,80,205,6);
    model->SetParameters(par0, par1, par2, 1000, 133, 8);
    model->SetParLimits(3,1,10000);
    model->SetParLimits(4,115,180);
    model->SetParLimits(5,4,25);
    fitres = m_hist->Fit("model","LQS","",80,205);
    
    if ( !( (model->GetParameter(5)>4.1 && model->GetParameter(5)<24.9) &&
	    (model->GetParameter(4)>116 && model->GetParameter(4)<179) ) ) {
      
      //---> Fit failed: try another...
      
      m_hist->Fit("fitBkg","LQ","",100,250);
      double par0 = fitBkg->GetParameter(0);
      double par1 = fitBkg->GetParameter(1);
      double par2 = fitBkg->GetParameter(2);
      model->SetParameters(par0, par1, par2, 1000, 180, 8);
      model->SetParLimits(3,1,10000);
      model->SetParLimits(4,160,250);
      model->SetParLimits(5,4,20);
      fitres = m_hist->Fit("model","LQS","",100,250);	
      
      if ( !( (model->GetParameter(5)>4.1 && model->GetParameter(5)<19.9) &&
	      (model->GetParameter(4)>161 && model->GetParameter(4)<249) ) ) {	

	model->FixParameter(5, 0);

      } else {
	isGoodFit = true;
      }
    } else {
      isGoodFit = true;
    }    
  } else {
    isGoodFit = true;
  }




  m_mean  = std::make_pair( model->GetParameter(4), model->GetParError(4) );
  m_sigma = std::make_pair( model->GetParameter(5), model->GetParError(5) );
  double nS  =  model->Integral(0,250);
  double nSE =  model->GetParError(3);
  m_nsignal = std::make_pair( nS, nSE*nS ); 
  if (isGoodFit) m_status = 0;
  else { 
    verbose = true;
    subdir = "bad"; 
  }
  if(verbose){
    gStyle->SetOptFit(1112);
    gStyle->SetStatFormat("6.2g");
    gStyle->SetStatStyle(4001);
    TCanvas* cc = new TCanvas();
    m_hist->Draw();

    if( !exists(outputdir) ) create_directories( outputdir );
    if( !exists(outputdir+"/"+subdir) ) create_directories( outputdir+"/"+subdir );
    boost::filesystem::path outfile(outputdir+"/"+subdir+"/"+outfilename);

    cc->SaveAs( outfile.c_str() );
    cc->Close();
  }

}
