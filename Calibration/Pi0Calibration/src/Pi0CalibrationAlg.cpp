/*
 * =====================================================================================
 *
 *       Filename:  Pi0CalibrationAlg.cpp
 *
 *    Description:  main algorithm for pi0 calibration at LHCb online
 *
 *        Version:  1.0
 *        Created:  11/21/2016 01:53:44 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>
#include <fstream>

#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticlePropertySvc.h"

#include "CaloDet/DeCalorimeter.h"
#include "Kernel/CaloCellCode.h"
#include "CaloKernel/CaloVector.h"
#include "CaloDet/CellParam.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiAlg/GaudiAlg.h"

// from local
#include "Pi0Calibration/Pi0CalibrationAlg.h"
#include "Pi0Calibration/Pi0MassFiller.h"
#include "Pi0Calibration/Pi0MassFitter.h"
#include "Pi0Calibration/Pi0LambdaMap.h"

#include "TRandom.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph2D.h"
#include "TColor.h"
#include "TStyle.h" 
#include "TROOT.h" 
#include "TText.h" 
#include "TBox.h" 
#include "TArrow.h"
#include "TMath.h"
#include "TPostScript.h"

#include "RooRealVar.h"
#include "RooDataSet.h"

//from boost
#include <boost/filesystem.hpp>

#include "DetDesc/Condition.h"

using namespace boost;
using namespace boost::filesystem;
using namespace Calibration::Pi0Calibration;

DECLARE_COMPONENT( Pi0CalibrationAlg )

Pi0CalibrationAlg::Pi0CalibrationAlg(const std::string& name, ISvcLocator* pSvcLocator)
: GaudiAlgorithm( name, pSvcLocator ), m_myCond(NULL), m_beta()
{
       
  declareProperty( "tupleFileName", m_tupleFileName);
  declareProperty(     "tupleName", m_tupleName);
  declareProperty(     "outputDir", m_outputDir);
  declareProperty("lambdaFileName", m_lambdaFileName="");
  declareProperty("saveLambdaFile", m_saveLambdaFile);
  declareProperty("saveLambdaDBFile", m_saveLambdaDBFile);
  declareProperty(     "fitMethod", m_fitMethod="CHI2");
  declareProperty(      "filetype", m_filetype="ROOT");

}

Pi0CalibrationAlg::~Pi0CalibrationAlg(){ }

StatusCode Pi0CalibrationAlg::initialize(){
  
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() ) return sc;

  // find the pi0 mass value
  const LHCb::IParticlePropertySvc* ppsvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc" ) ;
  const LHCb::ParticleProperty* pi0 = ppsvc->find ( "pi0" ) ;
  if ( 0 == pi0 ) {return sc;} 
  m_pdgPi0 = pi0->mass();
  // import a list of calo cells
  DeCalorimeter* m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  const CaloVector<CellParam>& cells = m_calo->cellParams();
  for( CaloVector<CellParam>::const_iterator icell = cells.begin(); icell != cells.end(); icell++ ){
    LHCb::CaloCellID id = (*icell).cellID();
    if( !m_calo->valid( id ) || id.isPin() ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::OfflineMask) ) continue;
    m_cells.push_back( id );
    m_indices.push_back( id.index() );
  }

  // Get beta and gamma parameters from the DB, for ECorrections
  registerCondition("Conditions/Reco/Calo/PhotonECorrection", m_myCond, &Pi0CalibrationAlg::cacheConditionData);
  sc = runUpdate();
  if (sc.isFailure()) {
    return Error("Could not update conditions from the CondDB.", sc);
  }

  return sc;
}

StatusCode Pi0CalibrationAlg::execute(){
  StatusCode sc = StatusCode::SUCCESS;
  return sc;
}

StatusCode Pi0CalibrationAlg::finalize(){
  StatusCode sc = StatusCode::SUCCESS;

  //std::cout << "in finalize" << std::endl;
  if( !exists(m_tupleFileName) ) {
    std::cout << "no m_tupleFileName" << std::endl;
    return StatusCode::FAILURE;
  }
  // load the lambda map
  Pi0LambdaMap lMap( m_indices );
  if( exists(m_lambdaFileName) ) lMap.loadMapFromFile( m_lambdaFileName );
  if(msgLevel(MSG::INFO)) info() << " the lambda is loaded from file " << m_lambdaFileName << endmsg;
  // load the histogram
  Pi0MassFiller filler;
  if( m_filetype=="MMAP" ){
    if( msgLevel(MSG::INFO) ) info() << " read files created by MMapVector" << endmsg;
    filler =  Pi0MassFiller( m_tupleFileName, m_indices, lMap, m_beta, m_gamma);
  } else if( m_filetype=="ROOT" ){
    if( msgLevel(MSG::INFO) ) info() << " read normal ROOT files" << endmsg;
    filler =  Pi0MassFiller( m_tupleFileName, m_tupleName, m_indices, lMap, m_beta, m_gamma);
  } else if( m_filetype=="TH2D" ){
    if( msgLevel(MSG::INFO) ) info() << " read histo files" << endmsg;
    filler = Pi0MassFiller( m_tupleFileName );
  }else{
    err() << " File type not recognised " << m_filetype << endmsg;
    return StatusCode::FAILURE;
  }
  TH2* m_hists = filler.hists();

  std::map<unsigned int, std::pair<double,double> > meanValues;
  std::map<unsigned int, std::pair<double,double> > sigmaValues;
  std::map<unsigned int, std::pair<double,double> > npi0Values;
  meanValues.clear();
  sigmaValues.clear();
  npi0Values.clear();
  m_skippedCells.clear();
  
  if(msgLevel(MSG::INFO)) info() << " fit the pi0 mass distribution to get the initial lambda values and the resolutions in each region " << endmsg; 
  if(msgLevel(MSG::VERBOSE)) verbose() << " create new maps for all the cells " << endmsg;

  Pi0LambdaMap newMap( m_indices );
  for( auto& cell : m_cells ) {
    unsigned int seedindex = cell.index();

    TH1D* hist    = m_hists->ProjectionY(  ("S_ID"+std::to_string(seedindex)+"_ROW"+std::to_string(cell.row())+"_COL"+std::to_string(cell.col())).c_str(), seedindex+1, seedindex+1 );
    if( hist->GetEntries()==0 ) {m_skippedCells.push_back(cell); continue;}

    Pi0MassFitter fitter( hist, m_pdgPi0, 10.0 );
    std::string outfileName( "Calibration_Pi0_massfit");
    outfileName += "_"+cell.areaName();
    outfileName += "_ROW"+std::to_string( cell.row() );
    outfileName += "_COL"+std::to_string( cell.col() );
    outfileName += "_INDEX"+std::to_string( cell.index() );
    outfileName += ".pdf";
    if( m_fitMethod=="CHI2")
	fitter.chi2fit( m_outputDir+"/"+cell.areaName(), outfileName, msgLevel(MSG::VERBOSE) );

    meanValues[cell.index()]   = fitter.getMean();
    sigmaValues[cell.index()]  = fitter.getSigma();
    npi0Values[cell.index()]   = fitter.getNSignal();
  
    auto lold   = lMap.get_lambda( cell.index() );
    if( fitter.getStatus()==0 ){
      if( lold.first < 0.0) lold = std::make_pair(1.0, 0.0);
      auto lnew_value = m_pdgPi0/fitter.getMean().first;
      auto lnew_error = lnew_value*fitter.getMean().second/fitter.getMean().first;
      auto lambda_value = lold.first * lnew_value;
      auto lambda_error = lambda_value * std::sqrt( lnew_error/lnew_value*lnew_error/lnew_value+lold.second/lold.first*lold.second/lold.first  );
      newMap.setLambda( cell.index(), std::make_pair( lambda_value,lambda_error ) );
    }else{ 
      m_skippedCells.push_back( cell );
      if( !( lold.first<0.0 || lold.second<0.0) ) newMap.setLambda( cell.index(), lold );
    }
  }
  
  if(msgLevel(MSG::INFO)) info() << " save the updated lambda to the file " << m_saveLambdaFile << endmsg; 
  newMap.saveMapToFile( m_saveLambdaFile );
  newMap.saveMapToDBFile( m_saveLambdaDBFile );

  saveCells( m_skippedCells );
  saveValues(  meanValues, "mean" );
  saveValues( sigmaValues, "sigma" );
  saveValues( npi0Values, "npi0" );
  meanValues.clear();
  sigmaValues.clear();
  npi0Values.clear();
  return sc;
}

void Pi0CalibrationAlg::saveCells( const std::vector<LHCb::CaloCellID>& cells ){
  boost::filesystem::path filename( m_outputDir+"/"+"skipped_cells.txt" );
  std::ofstream ofile;
  ofile.open( filename.string(), std::ios::out | std::ios::trunc  );
  for( auto& cell : cells ) ofile << cell.calo() << " " << cell.area() << " " << cell.row() << " " << cell.col() << " " << cell.index() << "\n";
  ofile.close();
}
void Pi0CalibrationAlg::saveValues( const std::map<unsigned int, std::pair<double,double> >& values, std::string varname ){
  boost::filesystem::path filename( m_outputDir+"/"+varname+".txt" );
  std::ofstream ofile;
  ofile.open( filename.string(), std::ios::out | std::ios::trunc  );
  for( auto& value : values ) 
    ofile << value.first << "  " << value.second.first << "  " << value.second.second << "\n";
  ofile.close();
}

StatusCode Pi0CalibrationAlg::cacheConditionData() {

  if( m_myCond -> exists( "beta" ) )
    m_beta = &(m_myCond->paramAsDoubleVect("beta"));

  if( m_myCond -> exists( "offset" ) )
    m_gamma = &(m_myCond->paramAsDoubleVect("offset"));
 
  return StatusCode::SUCCESS;
}
