# echo "Setting Pi0Calibration v1r0 in /afs/cern.ch/user/z/zhxu/cmtuser/AlignmentDev_v11r0/Calibration/"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /afs/cern.ch/sw/contrib/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=Pi0Calibration -version=v1r0 -path=/afs/cern.ch/user/z/zhxu/cmtuser/AlignmentDev_v11r0/Calibration/  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

