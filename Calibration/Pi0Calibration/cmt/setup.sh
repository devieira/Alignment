# echo "Setting Pi0Calibration v1r0 in /afs/cern.ch/user/z/zhxu/cmtuser/AlignmentDev_v11r0/Calibration/"

if test "${CMTROOT}" = ""; then
  CMTROOT=/afs/cern.ch/sw/contrib/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=Pi0Calibration -version=v1r0 -path=/afs/cern.ch/user/z/zhxu/cmtuser/AlignmentDev_v11r0/Calibration/  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

